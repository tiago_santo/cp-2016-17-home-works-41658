#define _CRT_SECURE_NO_WARNINGS
#include "game.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void game_free(Game *game) {
	free(game);
}


//devolve a posi�ao do array 
//@retval -1 caso nao respeite os limites do array
//@retval pos caso respeite os limites do array
int getPos(Game* game,int row, int col) {


	int pos = (row*game->cols) + col;
	
	return pos;
}





int game_cell_is_alive(Game *game, size_t row, size_t col) {



	assert( row < game->rows && col < game->cols);

	int pos = getPos(game, row, col);


	if (game->board[pos] == '0')
		return 0;
	else
		return 1;

}

int game_cell_is_dead(Game *game, size_t row, size_t col) {


	return (!game_cell_is_alive(game, row, col));

}

void game_cell_set_alive(Game *game, size_t row, size_t col) {




	int pos = getPos(game, row, col);
	game->board[pos] = '1';
}


void game_cell_set_dead(Game *game, size_t row, size_t col) {


	int pos = getPos(game, row, col);
	game->board[pos] = '0';
}


Game* game_new() {


	Game* game = (Game*)malloc(sizeof(Game));

	return game;
}

int game_parse_board(Game *game, GameConfig *config) {
	
	assert(config != NULL);
	assert(config->input_file != NULL);

	FILE* fp = config->input_file;



	char * line = malloc(sizeof(char*));
	
	
	

	fgets(line, 60, fp);


	
	char * t = strtok(line, ":");

	
	t = strtok(NULL, t);

	

	game->rows = atoi(t);
	

	char * line2 = malloc(sizeof(char*));
	
	fgets(line2, 60, fp);
	char * t2 = strtok(line2, ":");
	t2 = strtok(NULL, t2);
	game->cols = atoi(t2);



	char c;
	int i = 0;

	int size = (game->rows*game->cols);

	char* board = malloc(size);
	//game->board = malloc(game->rows*game->cols);

	do {

		c = fgetc(fp);
		switch (c)
		{
		case '.':
			board[i] = '0';
			i++;
			break;
		case '#':
			board[i] = '1';
			i++;
			break;
		}

		

	} while (c != EOF);


	game->board = board;

	

	
			
	return 0;
}


void game_print_board(Game * game) {
	

	int r = game->rows;

	int c = game->cols;

	char * board = game->board;

	int size = r*c;
	int i;
	for (i = 0; i < size; i++)
	{
		if ((i + 1) % c == 0)
			printf("%c\n", board[i]);
		else
			printf("%c", board[i]);
		
	}

	printf("\n");

}



int allDead(Game * game) {

	int i = 0;
	int j = 0;

	int r = game->rows;
	int c = game->cols;


	for ( i = 0 ; i < r; i++)
	{
		for (j = 0; j < c; j++) {
			if (game_cell_is_alive(game, i, j))
				return 0;
		}

	}

	return 1;
}



int NumberNeighbourAlive(Game* game, int i, int j) {
	int count = 0;

	int r = game->rows;

	int c = game->cols;

	char * table = game->board;

	int a;
	int b;


	for ( a = -1; a < 2 ; a++)
		for ( b = -1; b < 2 ; b++)
		{

		
				int x = i + a;
				int y = j + b;

				if (x < 0) {
					x = r - 1;
				}
				if (y < 0) {
					y = c - 1;
				}
				if (x == r) {
					x = 0;
				}
				if (y == c) {
					y = 0;
				}
				
				if (a != 0 || b != 0)
				 if (game_cell_is_alive(game, x, y))
					 count++;

			
		}
		

	return count;
}


int sameState(char* ant, char * n,size_t size) {

	
	int i;
	for (i = 0; i < size; i++) {
		if (ant[i] != n[i])
			return 0;
	}
	return 1;
}



int game_tick(Game * game) {

	int r = game->rows;
	int c = game->cols;

	size_t size = r*c;

	Game * temp = game_new();
	temp->board = malloc(sizeof(char)*size);
	
	int a;
	for (a = 0; a < size; a++)
	{
		temp->board[a] = game->board[a];
	}
	temp->rows = r;
	temp->cols = c;


	int i;
	int j;
	for ( i = 0; i < r; i++) {
		for ( j = 0; j < c; j++) {

			

			if (game_cell_is_alive(game, i, j)) {
				if (NumberNeighbourAlive(game, i, j) < 2) {
					game_cell_set_dead(temp, i, j);
				}
				else if (NumberNeighbourAlive(game, i, j) > 3) {
					game_cell_set_dead(temp, i, j);
				}
				else if (NumberNeighbourAlive(game, i, j) == 3 || NumberNeighbourAlive(game, i, j) == 2) {
					game_cell_set_alive(temp, i, j);
				}
			}
			if (game_cell_is_dead(game, i, j)) {
				if (NumberNeighbourAlive(game, i, j) == 3) {
					game_cell_set_alive(temp, i, j);
				}
			}

		}
		
	}

	
	for ( a = 0; a < size; a++)
	{
		game->board[a] = temp->board[a];
	}

	game_free(temp);
	return 0;

}
