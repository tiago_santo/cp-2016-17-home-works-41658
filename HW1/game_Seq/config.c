
#include "config.h"
#include <string.h>
#include <unistd.h>
#include <stdio.h>



void game_config_free(GameConfig * config) {
	free(config);
}

size_t game_config_get_generations(GameConfig * config) {

	return config->generations;
}


GameConfig* game_config_new_from_cli(int argc, char* argv[]) {

	GameConfig* config = (GameConfig*)malloc(sizeof(GameConfig*)*argc);

	int c;

	config->generations = 20;

	while ((c = getopt(argc, argv, "dn:s")) != -1) {
		switch (c)
		{
		case 'd':
			config->debug = 1;
			break;
		case 'n':
			if(optarg == NULL)
				config->generations = 20;
			else
				config->generations = atoi(optarg);
			break;
		case 's':
			config->silent = 1;
			break;
		case '?':
			break;

		}
	}

	if (optind == 1)
		config->generations = 20;

	
	FILE* file = fopen(argv[optind], "r");

	
	config->input_file = file;

	


	return config;


}

int game_config_get_silent(GameConfig * config) {
	return config->silent;
}


int game_config_get_debug(GameConfig * config) {
	return config->debug;
}



