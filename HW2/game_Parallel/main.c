
#include <stdlib.h>
#include <stdio.h>
#include "game.h"
#include <string.h>






int main(int argc, char*argv[]) {


	

	Game *game = game_new();
	
	GameConfig * config = game_config_new_from_cli(argc, argv);


	int p = game_parse_board(game,config);


	int size = game->cols*game->rows;

	char* ant = malloc(sizeof(char*)*size);




	int i;
	
	game_print_board(game);

	
	if (config->debug == 1) {
		
		
		for (i = 1; i <= config->generations; i++)
		{
			
			
			if (i > 1) {
				if (sameState(ant, game->board, size))
					break;
			}
			if (allDead(game))
				break;
			strncpy(ant, game->board, size);
			game_tick(game);

			printf("%d\n", i);
			game_print_board(game);
			

			
				
			
		}
		printf("\n");


	}
	else {

		

		int state = 0;

		
		
		for (i = 0; i < config->generations; i++)
		{
			state = i;
			if (i > 1) {
				if (sameState(ant, game->board, size))
					break;
			}
			if (allDead(game))
				break;
			strncpy(ant, game->board, size);
			game_tick(game); 
			
		}

		if (config->silent <= 0 ) {
			printf("%d\n", state);
			game_print_board(game);
			printf("\n");
		}


	}
	
	

	game_print_board(game);
	game_config_free(config);
	game_free(game);
	free(ant);
	

	return 0;
}
